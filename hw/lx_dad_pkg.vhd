library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

-- Entities within lx_dad

package lx_dad_pkg is

	component clockgen
	port
	(
		-- inputs
		clk_i 	: in std_logic;
		reset_i	: in std_logic;
		sck_i	: in std_logic;
		SDI		: in std_logic;
		-- outputs
		phi_1	: out std_logic;
		phi_2 	: out std_logic;
		phi_st	: out std_logic;
		ph_rst 	: out std_logic;
		--LED		: out std_logic;
		sck_o	: out std_logic;
		cnv_o	: out std_logic;
		

		mem_o		: out std_logic_vector(31 downto 0);
		
		--memory related outputs
		addr_o	: out std_logic_vector(10 downto 0);
		bls_o	: out std_logic_vector(3 downto 0);
		ce_o 	: out std_logic;

		-- mem related inputs

		addr_i	: in std_logic_vector(3 downto 0);
		data_i	: in std_logic_vector(31 downto 0);
		ce_i	: in std_logic;
		bls_i	: in std_logic_vector(3 downto 0);
		data_o	: out std_logic_vector(31 downto 0)
		

	);
	end component;

	component bus_sensor is
	port
	(
		-- Clock
		clk_i        : in std_logic;
		-- Chip enable
		ce_i         : in std_logic;
		-- Global Reset
		reset_i      : in std_logic;
		-- Master CPU peripheral bus
		bls_i        : in std_logic_vector(3 downto 0);
		address_i    : in std_logic_vector(10 downto 0);
		data_i       : in std_logic_vector(31 downto 0);
		data_o       : out std_logic_vector(31 downto 0);

		led		: out std_logic;
		
		-- Memory wiring for internal state automata use
		ce_a_i   : in std_logic;
		adr_a_i  : in std_logic_vector(10 downto 0);
		bls_a_i  : in std_logic_vector(3 downto 0);
		dat_a_i  : in std_logic_vector(31 downto 0)
		-- Non bus signals
		--
		-- Add there external component signals
  	);
	end component;
	
	component sensor_mem is
	port
	(
		-- Memory wiring for internal state automata use
		clk_i  : in std_logic;
		ce_i   : in std_logic;
		adr_i  : in std_logic_vector(10 downto 0);
		bls_i  : in std_logic_vector(3 downto 0);
		dat_i  : in std_logic_vector(31 downto 0);
		dat_o  : out std_logic_vector(31 downto 0);
		-- Memory wiring for Master CPU
		clk_m  : in std_logic;
		en_m   : in std_logic;
		we_m   : in std_logic_vector(3 downto 0);
		addr_m : in std_logic_vector(10 downto 0);
		din_m  : in std_logic_vector(31 downto 0);
		dout_m : out std_logic_vector(31 downto 0)
	);
	end component;

	component lx_adc_if is 
	generic
	(
		adc_res   	: positive := 18;
		conv_cycles	: integer := 85
	);
	port
	(
		clk_i	: in std_logic;
		rst_i	: in std_logic;
		conv_start	: in std_logic;
	
		sck_o	: out std_logic;
		cnv_o	: out std_logic;
		data_o	: out std_logic_vector((adc_res-1) downto 0);
		drdy_o	: out std_logic;
		
		sck_i	: in std_logic;
		SDI		: in std_logic
	);
	end component;
	
	-- D sampler (filtered, 2 cycles)
	component dff2
	port
	(
		clk_i   : in std_logic;
		d_i     : in std_logic;
		q_o     : out std_logic
	);
	end component;

	-- D sampler (filtered, 3 cycles)
	component dff3
	port
	(
		clk_i   : in std_logic;
		d_i     : in std_logic;
		q_o     : out std_logic
	);
	end component;

	-- Counter - divider
	component cnt_div
	generic (
		cnt_width_g : natural := 8
	);
	port
	(
		clk_i     : in std_logic;
		en_i      : in std_logic;
		reset_i   : in std_logic;
		ratio_i   : in std_logic_vector(cnt_width_g-1 downto 0);
		q_out_o   : out std_logic
	);
	end component;

	-- Clock Cross Domain Synchronization Elastic Buffer/FIFO
	component lx_crosdom_ser_fifo
	generic
	(
		fifo_len_g   : positive := 8;
		sync_adj_g   : integer := 0
	);
	port
	(
		-- Asynchronous clock domain interface
		acd_clock_i  : in std_logic;
		acd_miso_i   : in std_logic;
		acd_sync_i   : in std_logic;
		-- Clock
		clk_i        : in std_logic;
		reset_i      : in std_logic;
		-- Output synchronous with clk_i
		miso_o       : out std_logic;
		sync_o       : out std_logic;
		data_ready_o : out std_logic
	);
	end component;

	--------------------------------------------------------------------------------
	-- MEMORY BUS
	--------------------------------------------------------------------------------

	-- Measurement register
	component measurement_register
	generic
	(
		id_g   : std_logic_vector(31 downto 0) := (others => '0')
	);
	port
	(
		-- Clock
		clk_i    : in std_logic;
		-- Reset
		reset_i  : in std_logic;
		-- Chip enable
		ce_i     : in std_logic;
		-- Switch
		switch_i : in std_logic;
		-- Data bus
		data_i   : in std_logic_vector(31 downto 0);
		data_o   : out std_logic_vector(31 downto 0);
		-- Bus signals
		bls_i    : in std_logic_vector(3 downto 0)
	);
	end component;

	-- Example component interconnect
	component bus_example
	port
	(
		clk_i        : in std_logic;
		reset_i      : in std_logic;
		-- Master CPU peripheral bus
		address_i    : in std_logic_vector(11 downto 0);
		ce_i         : in std_logic;
		data_i       : in std_logic_vector(31 downto 0);
		data_o       : out std_logic_vector(31 downto 0);
		--
		bls_i        : in std_logic_vector(3 downto 0)

		-- Non bus signals
		--
		-- Add there externaly visible signals
	);
	end component;

	-- Dualported memory for example componenet
	component lx_example_mem
	port
	(
		-- Memory wiring for internal state automata use
		clk_i  : in std_logic;
		ce_i   : in std_logic;
		adr_i  : in std_logic_vector(9 downto 0);
		bls_i  : in std_logic_vector(3 downto 0);
		dat_i  : in std_logic_vector(31 downto 0);
		dat_o  : out std_logic_vector(31 downto 0);
		-- Memory wiring for Master CPU
		clk_m  : in std_logic;
		en_m   : in std_logic;
		we_m   : in std_logic_vector(3 downto 0);
		addr_m : in std_logic_vector(9 downto 0);
		din_m  : in std_logic_vector(31 downto 0);
		dout_m : out std_logic_vector(31 downto 0)
	);
	end component;

	-- Measurement interconnect
	component bus_measurement
	port
	(
		-- Clock
		clk_i     : in std_logic;
		-- Reset
		reset_i   : in std_logic;
		-- Chip enable
		ce_i      : in std_logic;
		-- Address
		address_i : in std_logic_vector(1 downto 0);
		-- Data bus
		data_i    : in std_logic_vector(31 downto 0);
		data_o    : out std_logic_vector(31 downto 0);
		-- Bus signals
		bls_i     : in std_logic_vector(3 downto 0)
	);
	end component;

	-- Register on the bus
	component bus_register is
	generic
	(
		-- Reset value
		reset_value_g : std_logic_vector(31 downto 0) := (others => '0');
		-- Width
		b0_g          : natural := 8;
		b1_g          : natural := 8;
		b2_g          : natural := 8;
		b3_g          : natural := 8
	);
	port
	(
		-- Clock
		clk_i         : in std_logic;
		-- Reset
		reset_i       : in std_logic;
		-- Chip enable
		ce_i          : in std_logic;
		-- Data bus
		data_i        : in std_logic_vector((b0_g+b1_g+b2_g+b3_g-1) downto 0);
		data_o        : out std_logic_vector((b0_g+b1_g+b2_g+b3_g-1) downto 0);
		-- Bus signals
		bls_i         : in std_logic_vector(3 downto 0)
	);
	end component;


	--------------------------------------------------------------------------------
	-- BRAM
	--------------------------------------------------------------------------------
	type BRAM_type is (READ_FIRST, WRITE_FIRST, NO_CHANGE);

	component xilinx_dualport_bram
	generic
	(
		byte_width    : positive := 8;
		address_width : positive := 8;
		we_width      : positive := 4;
		port_a_type   : BRAM_type := READ_FIRST;
		port_b_type   : BRAM_type := READ_FIRST
	);
	port
	(
		clka  : in std_logic;
		rsta  : in std_logic;
		ena   : in std_logic;
		wea   : in std_logic_vector((we_width-1) downto 0);
		addra : in std_logic_vector((address_width-1) downto 0);
		dina  : in std_logic_vector(((byte_width*we_width)-1) downto 0);
		douta : out std_logic_vector(((byte_width*we_width)-1) downto 0);
		clkb  : in std_logic;
		rstb  : in std_logic;
		enb   : in std_logic;
		web   : in std_logic_vector((we_width-1) downto 0);
		addrb : in std_logic_vector((address_width-1) downto 0);
		dinb  : in std_logic_vector(((byte_width*we_width)-1) downto 0);
		doutb : out std_logic_vector(((byte_width*we_width)-1) downto 0)
	);
	end component;

end lx_dad_pkg;

package body lx_dad_pkg is

end lx_dad_pkg;
