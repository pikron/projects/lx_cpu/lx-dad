library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lx_dad_pkg.all;

-- 8 kB memory for data read from sensor
-- Can be accessed from the Master CPU

entity sensor_mem is
	port
	(
		-- Memory wiring for internal state automata use
		clk_i  : in std_logic;
		ce_i   : in std_logic;
		adr_i  : in std_logic_vector(10 downto 0);
		bls_i  : in std_logic_vector(3 downto 0);
		dat_i  : in std_logic_vector(31 downto 0);
		dat_o  : out std_logic_vector(31 downto 0);
		-- Memory wiring for Master CPU
		clk_m  : in std_logic;
		en_m   : in std_logic;
		we_m   : in std_logic_vector(3 downto 0);
		addr_m : in std_logic_vector(10 downto 0);
		din_m  : in std_logic_vector(31 downto 0);
		dout_m : out std_logic_vector(31 downto 0)
	);
end sensor_mem;

architecture rtl of sensor_mem is
begin

I_RAMB: xilinx_dualport_bram
	generic map
	(
		we_width => 4,
		byte_width => 8,
		address_width => 11,
		port_a_type => READ_FIRST,
		port_b_type => READ_FIRST
	)
	port map
	(
		-- Internal state automata port
		clka => clk_i,
		rsta => '0',
		ena => ce_i,
		wea => bls_i,
		addra => adr_i,
		dina => dat_i,
		douta => dat_o,

		-- Master CPU port
		clkb => clk_m,
		rstb => '0',
		enb => en_m,
		web => we_m,
		addrb => addr_m,
		dinb => din_m,
		doutb => dout_m
	);

end rtl;