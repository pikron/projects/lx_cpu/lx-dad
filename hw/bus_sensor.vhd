library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lx_dad_pkg.all;

-- Connects sampling memory and SPI interface

entity bus_sensor is
	port
	(
		-- Clock
		clk_i        : in std_logic;
		-- Chip enable
		ce_i         : in std_logic;
		-- Global Reset
		reset_i      : in std_logic;
		-- Master CPU peripheral bus
		bls_i        : in std_logic_vector(3 downto 0);
		address_i    : in std_logic_vector(10 downto 0);
		data_i       : in std_logic_vector(31 downto 0);
		data_o       : out std_logic_vector(31 downto 0);

		led		: out std_logic;
		-- Memory wiring for internal state automata use
		ce_a_i   : in std_logic;
		adr_a_i  : in std_logic_vector(10 downto 0);
		bls_a_i  : in std_logic_vector(3 downto 0);
		dat_a_i  : in std_logic_vector(31 downto 0)
		-- Non bus signals
		--
		-- Add there external component signals
  );
end bus_sensor;

architecture Behavioral of bus_sensor is

	signal sensor_mem_ce_s   : std_logic;
	signal sensor_mem_ce_r   : std_logic;
	signal sensor_mem_bls_s  : std_logic_vector(3 downto 0);
	signal sensor_mem_dout_s : std_logic_vector(31 downto 0);
	
	signal led_i: std_logic:='1';
begin



			led <= led_i;

sensor_mem_instance: sensor_mem
	port map
	(
		-- Memory wiring for internal state automata use
		clk_i  => clk_i,
		ce_i   => ce_a_i,
		adr_i  => adr_a_i,
		bls_i  => bls_a_i,
		dat_i  => dat_a_i,
		dat_o  => open,
		-- Memory wiring for Master CPU
		clk_m  => clk_i,
		en_m   => sensor_mem_ce_s,
		we_m   => sensor_mem_bls_s,
		addr_m => address_i(10 downto 0),
		din_m  => data_i,
		dout_m => sensor_mem_dout_s
	);

decoder_logic: process(ce_i, address_i, bls_i)
        begin
		sensor_mem_ce_s <= '0';
		sensor_mem_bls_s <= (others => '0');

		if ce_i = '1' then --and address_i(11 downto 10) = "00" then
			sensor_mem_ce_s <= '1';
			sensor_mem_bls_s <= bls_i;
		end if;
	end process;

output_multiplexer: process(sensor_mem_ce_r, sensor_mem_dout_s)
        begin
		data_o <= (others => '0');

		if sensor_mem_ce_r = '1' then
			data_o <= sensor_mem_dout_s;
		end if;
	end process;

sync_update:
	process
	begin
		wait until clk_i = '1' and clk_i'event;

		sensor_mem_ce_r <= sensor_mem_ce_s;
	end process;


end Behavioral;
