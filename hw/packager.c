#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <endian.h>

/* This util converts bitstream to more friendly format
 * for select map interface with x16 line.
 */

/* Endianness */
#if __BYTE_ORDER == __LITTLE_ENDIAN
int source_be = 0;
#elif __BYTE_ORDER == __BIG_ENDIAN
int source_be = 1;
#else
#error Invalid endianness.
#endif

uint32_t swab32(uint32_t x)
{
	return x<<24 | x>>24 |
		(x & (uint32_t)0x0000ff00UL)<<8 |
		(x & (uint32_t)0x00ff0000UL)>>8;
}

uint8_t fpga_swap(uint8_t x)
{
	return ((x & (uint8_t)0x01) << 7) | ((x & (uint8_t)0x02) << 5) |
		((x & (uint8_t)0x04) << 3) | ((x & (uint8_t)0x08) << 1) |
		((x & (uint8_t)0x10) >> 1) | ((x & (uint8_t)0x20) >> 3) |
		((x & (uint8_t)0x40) >> 5) | ((x & (uint8_t)0x80) >> 7);
}

void __attribute__((noreturn)) error_usage(const char* argv0)
{
	printf("Usage: %s le|be <bin> <pkg>\n", argv0);
	exit(1);
}

void __attribute__((noreturn)) error_reason(const char* argv0, const char* reason)
{
	printf("ERROR: %s\n", reason);
	printf("Usage: %s le|be <bin> <pkg>\n", argv0);
	exit(1);
}

int main(int argc, char** argv)
{
	int target_be, i;
	uint32_t sz, szs;
	uint8_t hb, lb;
	char magic[4];
	FILE *fin, *fout;

	if (argc != 4)
		error_usage(argv[0]);

	if (!strcmp(argv[1], "le"))
		target_be = 0;
	else if (!strcmp(argv[1], "be"))
		target_be = 1;
	else
		error_usage(argv[0]);

	fin = fopen(argv[2], "r");
	if (!fin)
		error_reason(argv[0], "Failed to open input file.");

	fseek(fin, 0L, SEEK_END);
	sz = ftell(fin);
	fseek(fin, 0L, SEEK_SET);

	if (sz & 0x01)
		error_reason(argv[0], "Invalid size (not aligned).");

	fout = fopen(argv[3], "w+");
	if (!fout)
		error_reason(argv[0], "Failed to open output file.");

	/* If we're switching endianness, we need to swap the bytes */
	if (target_be != source_be)
		szs = swab32(sz);
	else
		szs = sz;

	/* Write magic */
	magic[0] = 'F';
	magic[1] = 'P';
	magic[2] = 'G';
	magic[3] = 'A';
	fwrite(&magic, 1, 4, fout);

	/* Write size */
	fwrite(&szs, 1, sizeof(uint32_t), fout);

	/* Binary must be aligned */
	for (i = 0; i < sz / 2; i++)
	{
		/* Bitstream is big endian */
		fread(&hb, 1, 1, fin);
		fread(&lb, 1, 1, fin);

		/* Swap the bites */
		hb = fpga_swap(hb);
		lb = fpga_swap(lb);

		/* Write it */
		if (target_be)
		{
			fwrite(&hb, 1, 1, fout);
			fwrite(&lb, 1, 1, fout);
		}
		else
		{
			fwrite(&lb, 1, 1, fout);
			fwrite(&hb, 1, 1, fout);
		}
	}

	fclose(fout);
	return 0;
}
