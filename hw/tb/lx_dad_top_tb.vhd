LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.all;

LIBRARY std;
USE std.textio.all;

ENTITY lx_dad_top_tb IS
END lx_dad_top_tb;

ARCHITECTURE behavior OF lx_dad_top_tb IS

    -- Component Declaration for the Unit Under Test (UUT)

    COMPONENT lx_dad_top
    PORT(
         --clk_cpu : IN  std_logic;
         clk_50m : IN  std_logic;
         cs0_xc : IN  std_logic;
         rd : IN  std_logic;
         bls : IN  std_logic_vector(3 downto 0);
         address : IN  std_logic_vector(15 downto 0);
         data : INOUT  std_logic_vector(31 downto 0);

         init : IN  std_logic;

         -- signal connected to external JK FF
         event_jk_j : out std_logic;
         -- signals to image sensor
         phi1       : out std_logic;
         phi2       : out std_logic;
         phi_rst    : out std_logic;
         LED_1      : out std_logic;
         sck_o      : out std_logic;
         cnv_o      : out std_logic;
         phist      : out std_logic;
         sck_i      : in std_logic;
         SDI        : in std_logic
    );
    END COMPONENT;


   --Inputs
   --signal clk_cpu : std_logic := '0';
	 signal clk_50m : std_logic := '0';
   signal cs0_xc : std_logic := '1';
   signal rd : std_logic := '1';
   signal bls : std_logic_vector(3 downto 0) := (others => '1');
   signal address : std_logic_vector(15 downto 0) := (others => '0');

   signal init : std_logic := '1';
	 --BiDirs

   signal data : std_logic_vector(31 downto 0);

   -- Clock period definitions
	 --constant clk_period_cpu : time := 13.8 ns;
   constant clk_period_50m : time := 20 ns;

   signal adc_sck : std_logic;

BEGIN

   -- Instantiate the Unit Under Test (UUT)
   uut: lx_dad_top PORT MAP (
          --clk_cpu => clk_cpu,
          clk_50m => clk_50m,
          cs0_xc => cs0_xc,
          rd => rd,
          bls => bls,
          address => address,
          data => data,

          init => init,

          event_jk_j => open,
          -- signals to image sensor
          phi1       => open,
          phi2       => open,
          phi_rst    => open,
          LED_1      => open,
          sck_o      => adc_sck,
          cnv_o      => open,
          phist      => open,
          sck_i      => adc_sck,
          SDI        => adc_sck
        );

   -- Clock process definitions
--   clk_cpu_process :process
--   begin
--		clk_cpu <= '1';
--		wait for clk_period_cpu/2;
--		clk_cpu <= '0';
--		wait for clk_period_cpu/2;
--   end process;

	 clk_50m_process :process
   begin
		clk_50m <= '1';
		wait for clk_period_50m/2;
		clk_50m <= '0';
		wait for clk_period_50m/2;
   end process;


   -- Stimulus process
   stim_proc: process
   begin
      -- External ModelSim script

      wait;
   end process;

	setup_imem_process : process
	--	file imem_file : text open READ_MODE is "imem.bits";
	--	variable my_line : LINE;
	--	variable bits_line : LINE;
	--	variable mem_location : bit_vector(31 downto 0);
	--	variable imem_fill_addr : natural range 0 to 2**8-1 := 0;

        procedure  mcu_write(
                         constant addr: std_logic_vector(15 downto 0);
                         constant wrdata: std_logic_vector(31 downto 0)
                         ) is
        begin
		wait until clk_50m'event and clk_50m = '1';
		wait until clk_50m'event and clk_50m = '0';
		rd <= '1';
		address <= addr;
		data <= wrdata;
		bls <= "0000";
		cs0_xc <= '0';
		wait until clk_50m'event and clk_50m = '1';
		wait until clk_50m'event and clk_50m = '1';
		wait until clk_50m'event and clk_50m = '1';
		cs0_xc <= '1';
		bls <= "1111";
		data <= (others => 'Z');
		wait until clk_50m'event and clk_50m = '1';
        end mcu_write;

	begin

		-- Assert LX_DAD system reset for 3 clock cycles
		wait until clk_50m'event and clk_50m = '1';
		init <= '0';
		wait until clk_50m'event and clk_50m = '1';
		wait until clk_50m'event and clk_50m = '1';
		wait until clk_50m'event and clk_50m = '1';
		init <= '1';

		-- Fill Tumbl instruction memory
		--fill_loop: while not endfile(imem_file) loop
		--	wait until clk_50m'event and clk_50m = '1';
		--	cs0_xc <= '1';
		--	rd <= '1';
		--	bls <= "1111";
		--	wait until clk_50m'event and clk_50m = '1';
		--	address <= std_logic_vector(to_unsigned(imem_fill_addr, 16));
		--	readline(imem_file, bits_line);
		--	read(bits_line, mem_location);
		--	data <= to_stdLogicVector(mem_location);
		--	bls <= "0000";
		--	cs0_xc <= '0';
		--	imem_fill_addr := imem_fill_addr + 1;
		--	wait until clk_50m'event and clk_50m = '1';
		--	wait until clk_50m'event and clk_50m = '1';
		--	cs0_xc <= '1';
		--	rd <= '1';
		--	bls <= "1111";
		--end loop fill_loop;

		-- Write to example bus memory
		mcu_write(x"1000", x"00000000");

		mcu_write(x"1001", x"00000009"); -- 9
		mcu_write(x"1002", x"00000020"); -- 399
		mcu_write(x"1003", x"00000020"); -- 409
		mcu_write(x"1004", x"00000020"); -- 399
		mcu_write(x"1005", x"00000009"); -- 9
		mcu_write(x"1006", x"00000040"); -- 599
		mcu_write(x"1007", x"00000050"); -- 609
		mcu_write(x"1008", x"00002000"); -- 5023999
		mcu_write(x"1009", x"00000008"); -- 499

		mcu_write(x"100C", x"00000010");

		mcu_write(x"1000", x"00000009"); -- standard
		-- mcu_write(x"1000", x"00000011"); -- leakage
		-- mcu_write(x"1000", x"00000021"); -- multi

		-- Simulate external master accesses example bus memory
		xmem_loop: loop
			wait until clk_50m'event and clk_50m = '1';
			wait until clk_50m'event and clk_50m = '1';
			address <= x"0000";
			rd <= '0';
			cs0_xc <= '0';
			wait until clk_50m'event and clk_50m = '1';
			wait until clk_50m'event and clk_50m = '1';
			wait until clk_50m'event and clk_50m = '1';
			wait until clk_50m'event and clk_50m = '1';
			cs0_xc <= '1';
			rd <= '1';
			bls <= "1111";
		end loop xmem_loop;

		wait;

	end process;

END;
