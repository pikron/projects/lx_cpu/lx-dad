#include <system_def.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <inttypes.h>
#include <stddef.h>
#include <utils.h>
#include <cmd_proc.h>
#include <hal_gpio.h>
#include <hal_mpu.h>
#include <hal_machperiph.h>
#include <LPC17xx.h>
#include <spi_drv.h>
#include <lt_timer.h>

#include <ul_log.h>
#include <ul_logreg.h>

#include "appl_defs.h"
#include "appl_fpga.h"

FILE *cmd_io_as_direct_file(cmd_io_t *cmd_io, const char *mode)
{
  if (cmd_io->priv.ed_line.io_stack)
    cmd_io = cmd_io->priv.ed_line.io_stack;

  return cmd_io_as_file(cmd_io, mode);
}

int cmd_do_test_memusage(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  void *maxaddr;
  char str[40];

  maxaddr = sbrk(0);

  snprintf(str,sizeof(str),"memusage maxaddr 0x%08lx\n",(unsigned long)maxaddr);
  cmd_io_write(cmd_io,str,strlen(str));

  return 0;
}

int cmd_do_test_adc(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  FILE *F;

  F = cmd_io_as_direct_file(cmd_io, "r+");
  if (F == NULL)
    return CMDERR_EIO;

  fprintf(F, "ADC: %ld %ld %ld %ld %ld\r\n", (LPC_ADC->DR[0] & 0xFFF0) >> 4,
         (LPC_ADC->DR[1] & 0xFFF0) >> 4,
         (LPC_ADC->DR[2] & 0xFFF0) >> 4,
         (LPC_ADC->DR[3] & 0xFFF0) >> 4,
         (LPC_ADC->DR[7] & 0xFFF0) >> 4);
  fclose(F);
  return 0;
}

#ifdef APPL_WITH_DISTORE_EEPROM_USER
int cmd_do_test_distore(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  appl_distore_user_set_check4change();
  return 0;
}

int cmd_do_test_diload(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  appl_distore_user_restore();
  return 0;
}
#endif /*APPL_WITH_DISTORE_EEPROM_USER*/

int cmd_do_test_loglevel_cb(ul_log_domain_t *domain, void *context)
{
  char s[30];
  cmd_io_t *cmd_io = (cmd_io_t *)context;

  s[sizeof(s)-1]=0;
  snprintf(s,sizeof(s)-1,"%s (%d)\r\n",domain->name, domain->level);
  cmd_io_puts(cmd_io, s);
  return 0;
}

int cmd_do_test_loglevel(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res=0;
  char *line;
  line = param[1];

  if(!line||(si_skspace(&line),!*line)) {
    ul_logreg_for_each_domain(cmd_do_test_loglevel_cb, cmd_io);
  } else {
    res=ul_log_domain_arg2levels(line);
  }

  return res>=0?0:CMDERR_BADPAR;
}

int cmd_do_spimst_blocking(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res;
  int opchar;
  char *p=param[3];
  int spi_chan = (int)(intptr_t)des->info[0];
  uint8_t *tx_buff = NULL;
  uint8_t *rx_buff = NULL;
  long addr = 0;
  int len = 0;
  spi_drv_t *spi_drv;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar!=':')
    return -CMDERR_OPCHAR;

  if(spi_chan==-1)
    spi_chan=*param[1]-'0';

  spi_drv = spi_find_drv(NULL, spi_chan);
  if(spi_drv==NULL)
    return -CMDERR_BADSUF;

  p=param[3];

  si_skspace(&p);
  if(isdigit((int)*p)){
    if(si_long(&p,&addr,16)<0) return -CMDERR_BADPAR;
  }
  if(si_fndsep(&p,"({")<0) return -CMDERR_BADSEP;

  if((res=si_add_to_arr(&p, (void**)&tx_buff, &len, 16, 1, "})"))<0)
    return -CMDERR_BADPAR;

  rx_buff=malloc(len);

  res=-1;
  if(rx_buff!=NULL)
    res = spi_transfer_with_mode(spi_drv, addr, len, tx_buff, rx_buff, SPI_MODE_3);

  if(res < 0) {
    printf("SPI! %02lX ERROR\n",addr);
  } else {
    int i;
    printf("SPI! %02lX ",addr);
    printf("TX(");
    for(i=0;i<len;i++) printf("%s%02X",i?",":"",tx_buff[i]);
    printf(") RX(");
    for(i=0;i<len;i++) printf("%s%02X",i?",":"",rx_buff[i]);
    printf(")");
    printf("\n");
    return 0;
  }

  if(rx_buff)
    free(rx_buff);
  if(tx_buff)
    free(tx_buff);

  return 0;
}

int sdram_access_test(void)
{
  unsigned int *ptr;
  unsigned int pattern;
  size_t ramsz = SDRAM_SIZE;
  size_t cnt;
  lt_mstime_t tic;
  size_t blksz, i;

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0x12abcdef;
  for (cnt = ramsz/sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;) {
    *(ptr++) = pattern;
    pattern = pattern + 0x87654321;
  }

  lt_mstime_update();
  printf("SDRAM write %d ms\n", (int)(lt_msdiff_t)(actual_msec - tic));

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0x12abcdef;
  for (cnt = ramsz/sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;) {
    if(*ptr != pattern) {
      printf("SDRAM error modify at %p (%08x)\n", ptr, *ptr ^ pattern);
      return -1;
    }
    *(ptr++) = ~pattern;
    pattern = pattern + 0x87654321;
  }

  lt_mstime_update();
  printf("SDRAM modify %d ms\n", (int)(lt_msdiff_t)(actual_msec - tic));

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0x12abcdef;
  for (cnt = ramsz/sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;) {
    if(*(ptr++) != ~pattern) {
      printf("SDRAM error read at %p (%08x)\n", ptr, *ptr ^ pattern);
      return -1;
    }
    pattern = pattern + 0x87654321;
  }

  lt_mstime_update();
  printf("SDRAM read %d ms\n", (int)(lt_msdiff_t)(actual_msec - tic));

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0;
  for (cnt = ramsz/sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;) {
    pattern += *(ptr++);
  }

  lt_mstime_update();
  printf("SDRAM sum %d ms res 0x%08x\n", (int)(lt_msdiff_t)(actual_msec - tic), pattern);

  for (blksz=1; blksz < 256 ; blksz *= 2) {
    lt_mstime_update();
    tic = actual_msec;

    pattern = 0;
    for (cnt = ramsz/sizeof(*ptr); cnt; cnt -= blksz) {
      ptr = (typeof(ptr))SDRAM_BASE;
      //ptr = (typeof(ptr))cmd_do_test_memusage;
      //ptr = (typeof(ptr))&ptr;
      for (i = blksz; i--; )
        pattern += *(ptr++);
    }
    lt_mstime_update();
    printf("SDRAM sum %d blksz %d ms res 0x%08x\n", (int)(lt_msdiff_t)(actual_msec - tic), (int)blksz, pattern);
  }

  return 0;
}

int cmd_do_testsdram(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  sdram_access_test();
  return 0;
}

int cmd_do_testmpu(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int i;
  int reg_cnt = hal_mpu_region_count();
  uint32_t ra, rsz;

  for (i = 0; i < reg_cnt; i++) {
    MPU->RNR = i;
    ra = MPU->RBAR & MPU_RBAR_ADDR_Msk;
    rsz = (1 << (__mfld2val(MPU_RASR_SIZE_Msk, MPU->RASR) + 1)) - 1;
    printf("R%d %08lX..%08lX %08lX %08lX\n", i, ra, ra + rsz, MPU->RBAR, MPU->RASR);
  }

  /*printf("IAP version %08X\n", lpcisp_read_partid());*/

  return 0;
}

int cmd_do_goaddr(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long addr;

  p = param[1];

  si_skspace(&p);
  if(si_ulong(&p,&addr,16)<0) return -CMDERR_BADPAR;

  printf("Jump to address %08lX\n\n", addr);

  ((void(*)(void))addr)();

  return 0;
}

typedef struct dad_status_bit_des_t {
  unsigned int mask;
  const char *label;
} dad_status_bit_des_t;

dad_status_bit_des_t dad_status_bit_des[] = {
  {FPGA_DAD_CTRL_CR_m,  "CR"},
  {FPGA_DAD_CTRL_SR_m,  "SR"},
  {FPGA_DAD_CTRL_BANK_m,"BANK"},
  {FPGA_DAD_CTRL_STD_m, "STD"},
  {FPGA_DAD_CTRL_LEAK_m,"LEAK"},
  {FPGA_DAD_CTRL_MULT_m,"MULT"},
  {FPGA_DAD_CTRL_FIN_m, "FIN"},
  {0, NULL}
};

int cmd_do_dadstatus(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  uint32_t ctrlstat  = *fpga_dad_ctrl_reg;
  volatile uint32_t *timreg  = fpga_dad_sens_timing;
  int val;
  int i;
  FILE *F;
  dad_status_bit_des_t *bdes;

  F = cmd_io_as_direct_file(cmd_io, "r+");
  if (F == NULL)
    return CMDERR_EIO;

  fprintf(F, "DAD_STATUS=0x%02"PRIx32"\r\n", ctrlstat);

  for (bdes = dad_status_bit_des; bdes->mask; bdes++) {
    fprintf(F, " %*s", (int)strlen(bdes->label),
              bdes->mask & ctrlstat? bdes->label: "-");
  }
  fprintf(F, "\r\n");

  for (i = 1; i <= 9; i++) {
    val = timreg[i - 1];
    fprintf(F, "TIMREG%d=%d\r\n", i, val);
  }

  fclose(F);
  return 0;
}

int cmd_do_single(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  volatile uint32_t *testaddr;
  int32_t values[2048];
  uint32_t status, status_old, i;
  int32_t val;
  FILE *F;

  F = cmd_io_as_direct_file(cmd_io, "r+");
  if (F == NULL)
    return CMDERR_EIO;

  testaddr  = fpga_dad_ctrl_reg;
  status_old = *testaddr;
  //while((status_old & (1<<2))==(status & (1<<2))) status=*testaddr;
  //while (*testaddr &(1<<6));
  *testaddr = FPGA_DAD_CTRL_LEAK_m | FPGA_DAD_CTRL_SR_m;
  while (!(*testaddr & FPGA_DAD_CTRL_FIN_m))
    fprintf(F, "\r\ncekam\r\n");

  status = *testaddr;
  if (status & FPGA_DAD_CTRL_BANK_m) {
    testaddr =  fpga_dad_result_bank1;
  } else {
    testaddr =  fpga_dad_result_bank0;
  }
  for(i=0;i<1024;i++){
  //   values[i]=*testaddr++;
    val=*testaddr++;
    val+=(1<<17);
    val&=(1<<18)-1;
    val -= (1<<17);
    values[i]=val;
  }
  if (status & FPGA_DAD_CTRL_BANK_m) {
    fprintf(F, "bank1\r\n");
  } else {
    fprintf(F, "bank0\r\n");
  }
  for (i=0;i<1024;i++){
    if (i==1024){
      fprintf(F, "bank1\r\n");
    }
    fprintf(F, "%"PRId32"\r\n",values[i]);
  }
  fprintf(F, "end\r\n");

  fclose(F);

  return 0;
}

int cmd_do_running_meas(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]){
  volatile uint32_t *testaddr;
  int32_t values[1024];
  uint32_t status, status_old, i;
  int32_t val;
  int32_t wcycles = 0;
  FILE *F;

  F = cmd_io_as_direct_file(cmd_io, "r+");
  //F = stdout;
  if (F == NULL)
    return CMDERR_EIO;

  testaddr  = fpga_dad_ctrl_reg;
  status_old = *testaddr;
  status = *testaddr;
  while((status_old & FPGA_DAD_CTRL_BANK_m) == (status & FPGA_DAD_CTRL_BANK_m)) {
    status = *testaddr;
    wcycles++;
    if (wcycles > 10000000) {
      fprintf(F, "scan wait timeout\r\n");
      fprintf(F, "timeout status 0x%02"PRIx32" old 0x%02"PRIx32"\r\n",
              status, status_old);
      fclose(F);
      return CMDERR_TIMEOUT;
    }
  }
  if (status & FPGA_DAD_CTRL_BANK_m) {
    testaddr =  fpga_dad_result_bank1;
  } else {
    testaddr =  fpga_dad_result_bank0;
  }
  for(i=0; i<1024; i++) {
  //   values[i]=*testaddr++;
    val = *testaddr++;
    val += (1 << 17);
    val &= (1 << 18) - 1;
    val -= (1 << 17);
    values[i] = +1 * val;
  }
  fprintf(F, "bank%d status 0x%02"PRIx32" old 0x%02"PRIx32
             " wait cycles %"PRId32"\r\n",
             status & FPGA_DAD_CTRL_BANK_m? 1: 0,
             status, status_old, wcycles);
   for (i=0;i<1024;i++){
     fprintf(F, "%"PRId32"\r\n",values[i]);
   }
   fprintf(F, "end\r\n");

   fclose(F);

   return 0;
}

int cmd_do_phasetime(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res;
  int opchar;
  long val;
  char *p;
  int tn = (int)(intptr_t)des->info[0];
  volatile uint32_t *paraddr;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;

  if(tn==-1)
    tn=*param[1]-'0';

  if ((tn < 1) || (tn > 16))
    return CMDERR_BADSUF;

  paraddr  = fpga_dad_sens_timing + tn - 1;

  if (opchar == '?')
    return cmd_opchar_replong(cmd_io, param, *paraddr, 0, 0);

  if (opchar != ':')
    return CMDERR_OPCHAR;

  p=param[3];
  if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
  si_skspace(&p);
  if(*p) return -CMDERR_GARBAG;
  *paraddr=val;

  return 0;
}

int cmd_do_init(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  FILE *F;
  uint32_t mode = (uint32_t)des->info[0];

  F = cmd_io_as_direct_file(cmd_io, "r+");

  fprintf(F, "init 0x%02"PRIx32"!\r\n", mode);
  volatile uint32_t *testaddr  = fpga_dad_sens_timing;

  *fpga_dad_ctrl_reg = mode & ~(FPGA_DAD_CTRL_CR_m | FPGA_DAD_CTRL_SR_m);

//  *testaddr++;
  *testaddr++ = 9;
  *testaddr++ = 399;
  *testaddr++ = 409;
  *testaddr++ = 399;
  *testaddr++ = 9;
  *testaddr++ = 599;
  *testaddr++ = 609;
  //*testaddr++ = 4879;
  *testaddr++ = 5023999;
  *testaddr=499;

  *fpga_dad_ctrl_reg = mode;

  fprintf(F, "OK!\r\n");
  fclose(F);
  return 0;
}

int cmd_do_send(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *end;
  volatile uint32_t *testaddr  = fpga_dad_ctrl_reg;
  int read;
  int p=(int) strtol(param[1], &end, 10);
  testaddr+=p;
  p=(int) strtol(end, &end, 10);
  *testaddr=p;
  read=*testaddr;
  printf("sent %d & read %d\n", p, read);
  return 0;
}

cmd_des_t const cmd_des_test_memusage={0, 0,
                        "memusage","report memory usage",cmd_do_test_memusage,
                        {0,
                         0}};

cmd_des_t const cmd_des_test_adc={0, 0,
                        "testadc","adc test",cmd_do_test_adc,
                        {0,
                         0}};

#ifdef APPL_WITH_DISTORE_EEPROM_USER
cmd_des_t const cmd_des_test_distore={0, 0,
                        "testdistore","test DINFO store",cmd_do_test_distore,
                        {0,
                         0}};

cmd_des_t const cmd_des_test_diload={0, 0,
                        "testdiload","test DINFO load",cmd_do_test_diload,
                        {0,
                         0}};
#endif /*APPL_WITH_DISTORE_EEPROM_USER*/

cmd_des_t const cmd_des_test_loglevel={0, 0,
                        "loglevel","select logging level",
                        cmd_do_test_loglevel,{}};

cmd_des_t const cmd_des_spimst={0, CDESM_OPCHR|CDESM_WR,
			"SPIMST","SPI master communication request",
			cmd_do_spimst_blocking,{(void*)0}};

cmd_des_t const cmd_des_spimstx={0, CDESM_OPCHR|CDESM_WR,
			"SPIMST#","SPI# master communication request",
			cmd_do_spimst_blocking,{(void*)-1}};

cmd_des_t const cmd_des_testsdram={0, 0,
			"testsdram","test SDRAM",
			cmd_do_testsdram,{(void*)0}};

cmd_des_t const cmd_des_testmpu={0, 0,
			"testmpu","test MPU",
			cmd_do_testmpu,{(void*)0}};

cmd_des_t const cmd_des_goaddr={0, 0,
			"goaddr","run from address",
			cmd_do_goaddr,{(void*)0}};

cmd_des_t const cmd_des_dadstatus={0, 0,
			"dadstatus", "show status of DAD subsystem",
			cmd_do_dadstatus, {(void*)0}};

cmd_des_t const cmd_des_zmer={0, 0,
			"zmer", "read values from spectrometer (2 readouts)",
			cmd_do_single, {(void*)0x12}};

cmd_des_t const cmd_des_scan={0, 0,
			"scan", "read values from spectrometer (2 readouts)",
			cmd_do_single, {(void*)0x10}};

cmd_des_t const cmd_des_phasetime={0, CDESM_OPCHR|CDESM_RW,
			"PHT#","Phase # time",
			cmd_do_phasetime,{(void*)-1}};

cmd_des_t const cmd_des_init={0, 0,
			"init", "initialize DAD converter for continuous reading",
			cmd_do_init, {(void*)(FPGA_DAD_CTRL_STD_m | FPGA_DAD_CTRL_CR_m)}};

cmd_des_t const cmd_des_initmulti={0, 0,
			"initmulti", "initialize DAD converter for multivalue read",
			cmd_do_init, {(void*)(FPGA_DAD_CTRL_MULT_m | FPGA_DAD_CTRL_CR_m)}};

cmd_des_t const cmd_des_sendvalue={0, 0,
			"send", "send value to given address",
			cmd_do_send, {(void*)0}};

cmd_des_t const cmd_des_running_meas = {0, 0,
			"run", "read values from spectrometer (running)",
			cmd_do_running_meas, {(void*)0}};

cmd_des_t const *const cmd_appl_tests[]={
  &cmd_des_test_memusage,
  &cmd_des_test_adc,
 #ifdef APPL_WITH_DISTORE_EEPROM_USER
  &cmd_des_test_distore,
  &cmd_des_test_diload,
 #endif /*APPL_WITH_DISTORE_EEPROM_USER*/
  &cmd_des_test_loglevel,
  &cmd_des_spimst,
  &cmd_des_spimstx,
  &cmd_des_testsdram,
  &cmd_des_testmpu,
  &cmd_des_goaddr,
  &cmd_des_dadstatus,
  &cmd_des_zmer,
  &cmd_des_scan,
  &cmd_des_phasetime,
  &cmd_des_init,
  &cmd_des_initmulti,
  &cmd_des_sendvalue,
  & cmd_des_running_meas,
  NULL
};

