#include <system_def.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cmd_proc.h>

#include "appl_defs.h"

extern cmd_des_t const *cmd_pxmc_base[];
extern cmd_des_t const *cmd_appl_specific[];
extern cmd_des_t const *cmd_appl_tests[];
extern cmd_des_t const *cmd_pxmc_ptable[];

cmd_des_t const **cmd_list;

cmd_des_t const cmd_des_help={
    0, 0,
    "help","prints help for commands",
    cmd_do_help,{(char*)&cmd_list}};


cmd_des_t const *cmd_list_main[]={
  &cmd_des_help,
  CMD_DES_INCLUDE_SUBLIST(cmd_appl_tests),
  CMD_DES_INCLUDE_SUBLIST(cmd_appl_specific),
 #ifdef CONFIG_PXMC
  CMD_DES_INCLUDE_SUBLIST(cmd_pxmc_base),
  CMD_DES_INCLUDE_SUBLIST(cmd_pxmc_ptable),
 #endif
  NULL
};

cmd_des_t const **cmd_list = cmd_list_main;

#ifndef APPL_WITH_SIM_POSIX
extern cmd_io_t cmd_io_usbcon;
extern cmd_io_t cmd_io_uartcon;
#else /*APPL_WITH_SIM_POSIX*/
extern cmd_io_t cmd_io_std_line;
#endif /*APPL_WITH_SIM_POSIX*/

int cmdproc_poll(void)
{
  int ret = 0;

 #ifndef APPL_WITH_SIM_POSIX
  ret |= cmd_processor_run(&cmd_io_uartcon, cmd_list_main);
  ret |= cmd_processor_run(&cmd_io_usbcon, cmd_list_main);
 #else /*APPL_WITH_SIM_POSIX*/
  ret |= cmd_processor_run(&cmd_io_std_line, cmd_list_main);
 #endif /*APPL_WITH_SIM_POSIX*/
}
