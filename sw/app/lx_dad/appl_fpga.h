#ifndef _APPL_FPGA_H
#define _APPL_FPGA_H

#include "appl_defs.h"

/* Register defines */

#define FPGA_DAD_CTRL_REG     0x80004000
#define FPGA_DAD_SENS_TIMING  0x80004004
#define FPGA_DAD_RESULT_BANK0 0x80008000
#define FPGA_DAD_RESULT_BANK1 0x80009000

extern volatile uint32_t *fpga_dad_ctrl_reg;
extern volatile uint32_t *fpga_dad_sens_timing;
extern volatile uint32_t *fpga_dad_result_bank0;
extern volatile uint32_t *fpga_dad_result_bank1;

/* Control/status register bits */

#define FPGA_DAD_CTRL_CR_m    0x01 /* Continuous read */
#define FPGA_DAD_CTRL_SR_m    0x02 /* Single read */
#define FPGA_DAD_CTRL_BANK_m  0x04 /* Bank 1 / Bank 0 */
#define FPGA_DAD_CTRL_STD_m   0x08 /* Standard operation */
#define FPGA_DAD_CTRL_LEAK_m  0x10 /* Leak measurement mode */
#define FPGA_DAD_CTRL_MULT_m  0x20 /* Multiple reads per single diode */
#define FPGA_DAD_CTRL_FIN_m   0x40 /* Single read finished */

/* Configuration defines */

#define FPGA_CONFIGURATION_FILE_ADDRESS 0xA1C00000

#define FPGA_CONF_SUCESS              0
#define FPGA_CONF_ERR_RECONF_LOCKED   1
#define FPGA_CONF_ERR_RESET_FAIL      2
#define FPGA_CONF_ERR_WRITE_ERR       3
#define FPGA_CONF_ERR_CRC_ERR         4

int (*fpga_reconfiguaration_initiated)(void);
int (*fpga_reconfiguaration_finished)(void);

void fpga_init();
int fpga_configure();
int fpga_measure_bus_read();
int fpga_measure_bus_write();
void fpga_set_reconfiguration_lock(int lock);
int fpga_get_reconfiguration_lock();

#endif /*_APPL_FPGA_H*/
