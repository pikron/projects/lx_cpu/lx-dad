#include <stdio.h>
#include <cmd_proc.h>

int uart0Getch(void)
{
  return -1;
}

int uart0PutchNW(int ch)
{
  putchar(ch);
  return (unsigned char)ch;
}

cmd_des_t const *cmd_appl_tests[]={
  NULL
};

int appl_distore_init(void)
{
  return 0;
}
int appl_distore_user_set_check4change(void)
{
  return 0;
}

int appl_distore_user_restore(void)
{
  return 0;
}

int appl_distore_user_change_check(void)
{
  return 0;
}
